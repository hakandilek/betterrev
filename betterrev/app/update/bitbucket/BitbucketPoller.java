package update.bitbucket;

import play.Configuration;
import play.Logger;
import play.Play;
import play.libs.WS;
import play.libs.WS.Response;
import play.mvc.Http;
import update.BetterrevActor;
import update.pullrequest.ImportPullRequestsEvent;

/**
 * Polls the bitbucket API and delegates to the PullRequestImporter class
 */
public class BitbucketPoller extends BetterrevActor {

    private static final String API_URL = "https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/";

    @Override
    public void onReceive(Object message) {
        if (message instanceof PollBitbucketEvent) {
            Logger.debug("PollBitbucketEvent received.");

            Configuration configuration = Play.application().configuration();
            String owner = configuration.getString("owner");
            String project = configuration.getString("project");
            Logger.info(String.format("Polling bitbucket with owner '%s' and project '%s'", owner, project));

            Response response = WS.url(String.format(API_URL, owner, project)).get().get();
            if ((response.getStatus() != Http.Status.OK) || response.asJson() == null) {
                Logger.error("Bitbucket did not return a valid response on the current execution of run()...");
            } else {
                eventStream().publish(new ImportPullRequestsEvent(response.asJson(), project));
            }
        }
    }
}
