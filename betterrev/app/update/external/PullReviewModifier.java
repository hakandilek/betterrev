package update.external;

import models.PullReview;
import models.PullReviewEvent;
import models.PullReviewEventType;
import models.State;

/**
 * Service Class responsible for modifying a PullReview (and updating the lifecycle events) as specified by
 * actions occurring external to the DVCS polling.
 * <p/>
 * Developers Note: The following methods are effectively stubs which can be augmented with functionality
 * related to the underlying task.
 */
public final class PullReviewModifier {

    private PullReviewModifier() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    public static PullReview notifyMentor(String repositoryId, String requestId, String linkToExternalInfo) {
        PullReview pullReview = PullReview.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(pullReview, State.OPEN);

        pullReview.pullReviewEvents.add(
                new PullReviewEvent(PullReviewEventType.MENTOR_NOTIFIED, linkToExternalInfo));
        pullReview.state = State.PENDING_APPROVAL;
        pullReview.update();

        return pullReview;
    }

    public static PullReview terminatePullReview(String repositoryId, String requestId, String linkToExternalInfo) {
        PullReview pullReview = PullReview.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(pullReview, State.OPEN);

        pullReview.pullReviewEvents.add(
                new PullReviewEvent(PullReviewEventType.TERMINATED, linkToExternalInfo));
        pullReview.state = State.CLOSED;
        pullReview.update();

        return pullReview;
    }

    public static PullReview rejectPullReview(String repositoryId, String requestId, String linkToExternalInfo) {
        PullReview pullReview = PullReview.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(pullReview, State.PENDING_APPROVAL);

        pullReview.pullReviewEvents.add(
                new PullReviewEvent(PullReviewEventType.REJECTED, linkToExternalInfo));
        pullReview.state = State.CLOSED;
        pullReview.update();

        return pullReview;
    }

    public static PullReview approvePullReview(String repositoryId, String requestId, String linkToExternalInfo) {
        PullReview pullReview = PullReview.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(pullReview, State.PENDING_APPROVAL);

        pullReview.pullReviewEvents.add(
                new PullReviewEvent(PullReviewEventType.APPROVED, linkToExternalInfo));
        pullReview.state = State.ACCEPTED;
        pullReview.update();

        return pullReview;
    }

    public static PullReview mergePullReview(String repositoryId, String requestId, String linkToExternalInfo) {
        PullReview pullReview = PullReview.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(pullReview, State.ACCEPTED);

        pullReview.pullReviewEvents.add(
                new PullReviewEvent(PullReviewEventType.MERGED, linkToExternalInfo));
        pullReview.state = State.COMMITTED;
        pullReview.update();

        return pullReview;
    }

    private static void ensureExpectedStartState(PullReview pullReview, State expectedState) {
        if (pullReview.state != expectedState) {
            throw new IllegalStateException("Cannot transition from current State of " + pullReview.state);
        }
    }

}
