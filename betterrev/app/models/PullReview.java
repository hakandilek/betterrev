package models;

import com.avaje.ebean.validation.NotNull;
import com.google.common.base.Objects;

import org.joda.time.DateTime;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.utils.dao.BasicModel;
import utils.FetchConfiguration;
import utils.FetchDiffFilesString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;
import static utils.ExtractFilenamesFrom.extractFilenamesFromPullRequestDiffText;

/**
 * PullReview entity that is in essence a BetterRev enhanced version of a DVCS pullrequest.
 * <p/>
 * For information here that corresponds to bitbucket information the bitbucket pullrequest
 * is the canonical source. e.g. createdOn is the creation date of the pull request,
 * not this object.
 */
@Entity
public class PullReview extends Model implements BasicModel<Long> {

    private static final long serialVersionUID = 188525469548289315L;
    private static String owner = FetchConfiguration.owner();

    public static Model.Finder<Long, PullReview> find = new Model.Finder<>(Long.class, PullReview.class);

    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    @NotNull
    @Required
    public String repositoryId;

    @NotNull
    @Required
    public String pullRequestId;

    @NotNull
    @Required
    public String name;

    public String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    public List<PullReviewEvent> pullReviewEvents = new ArrayList<>();

    @NotNull
    @Enumerated(EnumType.STRING)
    public State state;

    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Tag> tags = new HashSet<>();

    @ManyToOne
    public User requester;

    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Mentor> mentors = new HashSet<>();

    @NotNull
    public DateTime createdOn;

    @NotNull
    public DateTime updatedOn;

    public PullReview(String repositoryId, String pullRequestId, String name, String description, User requester,
                      DateTime createdOn, DateTime updatedOn) {

        this.repositoryId = repositoryId;
        this.pullRequestId = pullRequestId;
        this.name = name;
        this.description = description;
        this.requester = requester;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
        this.state = State.NULL;

        String diffFilesString = FetchDiffFilesString.from(pullRequestUrlForOwner());
        this.mentors = evaluateMentorsFrom(diffFilesString);
    }

    public boolean wasUpdatedBefore(DateTime updated) {
        return this.updatedOn.isBefore(updated);
    }

    public boolean hasPullReviewEventWith(PullReviewEventType pullReviewEventType) {
        boolean eventFound = false;
        for (PullReviewEvent pullReviewEvent : pullReviewEvents) {
            if (pullReviewEvent.pullReviewEventType != null &&
                    pullReviewEvent.pullReviewEventType.equals(pullReviewEventType)) {
                eventFound = true;
                break;
            }
        }
        return eventFound;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(pullRequestId, repositoryId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        PullReview other = (PullReview) obj;
        return Objects.equal(pullRequestId, other.pullRequestId) && Objects.equal(repositoryId, other.repositoryId);
    }

    // https://bitbucket.org/api/2.0/repositories/AdoptOpenJDK/better-test-repo/pullrequests/1/diff
    public final String pullRequestUrlForOwner() {
        return String.format("https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/%s/diff",
                owner, repositoryId, pullRequestId);
    }

    // https://bitbucket.org/api/2.0/repositories/richardwarburton/better-test-repo/pullrequests/1/diff
    public String pullRequestUrl() {
        return String.format("https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/%s/diff",
                requester.bitbucketUserName, repositoryId, pullRequestId);
    }

    public final Set<Mentor> evaluateMentorsFrom(String inDiffFileString) {
        Set<String> filesChanged = extractFilenamesFromPullRequestDiffText(inDiffFileString);
        Set<Mentor> results = new HashSet<>();

        results.addAll(Mentor.findRelevantMentors(repositoryId, filesChanged));
        return results;
    }

    public String mentorEmailsUrl() {
        StringBuilder mentorEmailsUrlBuilder = new StringBuilder().append("mailto:");

        int numberOfEmailsAdded = 0;
        int numberOfMentors = mentors.size();

        for (Mentor mentor : mentors) {
            mentorEmailsUrlBuilder.append(mentor.email);

            numberOfEmailsAdded++;
            if (numberOfEmailsAdded < numberOfMentors) {
                mentorEmailsUrlBuilder.append(",");
            }
        }

        return mentorEmailsUrlBuilder.toString();
    }

    public String requestersRepositoryUrl() {
        return String.format("ssh://hg@bitbucket.org/%s/%s", requester.bitbucketUserName, repositoryId);
    }

    /**
     * Eg: 'corba', 'hotspot', '.'
     */
    public String openJdkRepoName() {
        String[] split = repositoryId.split("-");

        // Subrepo case
        if (split.length == 2) {
            return split[1];
        }

        // Top level repo case
        return ".";
    }

    public static PullReview findByBitbucketIds(String repositoryId, String requestId) {
        return find.where()
                .eq("repositoryId", repositoryId)
                .eq("pullRequestId", requestId)
                .findUnique();
    }

    public File webrevLocation() {
        String relativePath = String.format("public/webrevs/webrev-%s-%s/", repositoryId, pullRequestId);
        return new File(relativePath).getAbsoluteFile();
    }

    @Override
    public Long getKey() {
        return id;
    }

    @Override
    public void setKey(Long key) {
        id = key;
    }
}
