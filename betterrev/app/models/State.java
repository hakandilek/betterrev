package models;

/**
 * State of PullReview.
 */
public enum State {
    NULL,
    OPEN,
    PENDING_APPROVAL,
    ACCEPTED,
    CLOSED,
    COMMITTED
}
