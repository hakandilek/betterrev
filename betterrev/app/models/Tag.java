package models;

import static javax.persistence.GenerationType.IDENTITY;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.utils.dao.BasicModel;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Tag entity which represents a classification for a PullReview, and can
 * be used to group PullReviews (for example by Hack Day).
 */
@Entity
public class Tag extends Model implements BasicModel<Long> {

    // private static final long serialVersionUID = 188525469548289315L;

    // public static Model.Finder<Long, Tag> find = new Model.Finder<>(Long.class, Tag.class);

    @GeneratedValue(strategy=IDENTITY)
    @Id
    public Long id;

    @Basic
    @Required
    public String name;

    public String description;

    public Tag(String name) {
        this.name = name;
    }

    public Tag(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public Long getKey() {
        return id;
    }

    @Override
    public void setKey(Long key) {
        id = key;
    }

}
