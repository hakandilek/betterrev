package models;

import play.db.ebean.Model;
import play.utils.dao.BasicModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entity that represents a concept of Interest within Betterrev.
 */
@Entity
public class Interest extends Model implements BasicModel<Long> {

    private static final long serialVersionUID = 188525469548289315L;

    public static Finder<Long, Interest> find = new Finder<>(Long.class, Interest.class);

    @GeneratedValue(strategy=IDENTITY)
    @Id
    public Long id;

    public String path;

    public String project;

    public Interest(String path, String project) {
        this.path = path;
        this.project = project;
    }

    @Override
    public String toString() {
        return "Interest [id=" + id + ", path=" + path + ", project=" + project + "]";
    }

    public boolean caresAbout(String repository, Set<String> filePaths) {
        for (String path: filePaths) {
            if (repository.equals(project)
                    && path.matches(this.path)) {
                return true;
            }
        }
        return false;
    }

	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public void setKey(Long key) {
		id = key;
	}
    
}