package utils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.*;

public final class ExtractFilenamesFrom {
    private static final String FILES_ADDED_TO_PULL_REQUEST_PATTERN =  "\\+\\+\\+ b/([^ ]+)\\n";
    private static final String FILES_DELETED_FROM_PULL_REQUEST_PATTERN = "\\-\\-\\- a/([^ ]+)\\n";

    public static final Set<String> EMPTY_FILES_LIST = new HashSet<>();

    private ExtractFilenamesFrom() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    public static Set<String> extractFilenamesFromPullRequestDiffText(String diffFileString) {
        if (diffFileString == null) {
            return EMPTY_FILES_LIST;
        }

        Set<String> results = extractFilesUsing(FILES_ADDED_TO_PULL_REQUEST_PATTERN, diffFileString);
        results.addAll(extractFilesUsing(FILES_DELETED_FROM_PULL_REQUEST_PATTERN, diffFileString));

        return results;
    }

    public static Set<String> extractFilesUsing(String pattern, String diffFileString) {
        java.util.regex.Pattern addedFilesPattern = java.util.regex.Pattern.compile(pattern);
        Matcher addedFilesMatcher = addedFilesPattern.matcher(diffFileString);

        Set<String> results = new HashSet<>();
        while (addedFilesMatcher.find()) {
            results.add(addedFilesMatcher.group(1));
        }
        return results;
    }
}