package utils;

import play.Configuration;
import play.Play;

public final class FetchConfiguration {

    private static Configuration configuration = Play.application().configuration();

    private FetchConfiguration() {
        // Hide construction of utility class
    }

    public static final FetchConfiguration INSTANCE = new FetchConfiguration();

    public static FetchConfiguration getInstance() {
        return INSTANCE;
    }

    public static String owner() {
        return configuration.getString("owner");
    }

    public static String project() {
        return configuration.getString("project");
    }
}