package utils;

/**
 * Enum class that holds String patterns associated with betterrev project
 */
public enum Pattern {
    DEFAULT{
        @Override
        public String toString() {
            return "dd MMMM yyyy kk:mm:ss";
        }
    }
}
