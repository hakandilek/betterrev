// Comment to get more information during initialization
logLevel := Level.Warn

// The Typesafe repository
resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

// Use the Play sbt plugin for Play projects
addSbtPlugin("play" % "sbt-plugin" % "2.1.1")
//addSbtPlugin("play" % "sbt-plugin" % "2.1.2")
//addSbtPlugin("play" % "sbt-plugin" % "2.1.3")

// The following will work with sbt 0.13
addCommandAlias("unit-test", "test-only *Test")
 
addCommandAlias("system-test", "test-only *ST")
