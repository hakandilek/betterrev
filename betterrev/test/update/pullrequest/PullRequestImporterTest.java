package update.pullrequest;

import akka.actor.ActorRef;
import models.PullReview;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static models.PullReviewEventType.PULL_REVIEW_MODIFIED;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class PullRequestImporterTest extends AbstractPullRequestImporterTest {

    private static final String KARIANNA_CREATION = "2013-11-09T12:27:10.557977+00:00";

    @BeforeClass
    public static void setupActors() {
        ActorRef pullRequestImporterActor = actorRule.actorOf(PullRequestImporter.class);
        eventStream.subscribe(pullRequestImporterActor, ImportPullRequestsEvent.class);
    }

    @Before
    public void dropEntries() {
        for (PullReview review : PullReview.find.all())
            review.delete();
    }

    @Test
    public void importsNewPullRequest() throws Exception {
        Future<PullRequestsImportedEvent> response =
                actorRule.expectMsg(PullRequestsImportedEvent.class, SECONDS.toMillis(10));

        eventStream.publish(new ImportPullRequestsEvent(firstResponse, repositoryId));

        PullRequestsImportedEvent reply = Await.result(response, Duration.Inf());

        PullReview review = reply.getPullReviews().get(0);
        String name = "Change to Readme purely to create a new PR";
        String description = "New test PR";
        checkPullReviewDataWasUpdated(review, name, description, "2013-11-09T12:27:10.586683+00:00", "2", KARIANNA_CREATION);
        assertPullReviewEquals(review, repositoryId, "2", name, description);
        assertRequesterInfoEquals(review, "karianna", "Martijn Verburg");
    }

    @Test
    public void importAllReviews_validRepo_fullUrlIsBuiltSuccessfully() throws Exception {
        Future<PullRequestsImportedEvent> response =
                actorRule.expectMsg(PullRequestsImportedEvent.class, SECONDS.toMillis(5));

        eventStream.publish(new ImportPullRequestsEvent(firstResponse, repositoryId));

        PullRequestsImportedEvent reply = Await.result(response, Duration.Inf());

        List<PullReview> pullReviews = reply.getPullReviews();
        PullReview review = pullReviews.get(0);
        assertThat(review.pullRequestUrl(), is("https://bitbucket.org/api/2.0/repositories/karianna/better-test-repo/pullrequests/2/diff"));

        review = pullReviews.get(1);
        assertThat(review.pullRequestUrl(), is("https://bitbucket.org/api/2.0/repositories/richardwarburton/better-test-repo/pullrequests/1/diff"));
    }

    @Test
    public void updatesReview() throws Exception {
        // Load the old version of the pull review, so there's something to update
        Future<PullRequestsImportedEvent> response =
                actorRule.expectMsg(PullRequestsImportedEvent.class, SECONDS.toMillis(5));
        eventStream.publish(new ImportPullRequestsEvent(firstResponse, repositoryId));
        PullReview oldReview = Await.result(response, Duration.Inf()).getPullReviews().get(0);

        response = actorRule.expectMsg(PullRequestsImportedEvent.class, SECONDS.toMillis(5));
        eventStream.publish(new ImportPullRequestsEvent(secondResponse, repositoryId));
        PullReview newReview = Await.result(response, Duration.Inf()).getPullReviews().get(0);

        assertEquals(oldReview.id, newReview.id);

        assertTrue("Could not find Modified PullReviewEventType",
                   newReview.hasPullReviewEventWith(PULL_REVIEW_MODIFIED));

        String name = "Change to Readme purely to create a new PR - Updated";
        String description = "New test PR - Updated";
        checkPullReviewDataWasUpdated(newReview, name, description, "2013-11-09T12:55:41.177558+00:00", "2", KARIANNA_CREATION);
    }

    private static void checkPullReviewDataWasUpdated(PullReview review, String name, String description, String updatedAt, String id, String createdAt) {
        assertPullReviewEquals(review, repositoryId, id, name, description);
        assertTimePointsEquals(review, DateTime.parse(createdAt), DateTime.parse(updatedAt));
    }

    private static void assertTimePointsEquals(PullReview review, DateTime expectedCreated, DateTime expectedUpdated) {
        assertEquals("Wrong creation timepoint", expectedCreated.getMillis(), review.createdOn.getMillis());
        assertEquals("Wrong update timepoint", expectedUpdated.getMillis(), review.updatedOn.getMillis());
    }

    private static void assertPullReviewEquals(PullReview review, String repoId, String pullRequestId, String name, String description) {
        assertEquals(repoId, review.repositoryId);
        assertEquals(pullRequestId, review.pullRequestId);
        assertEquals(name, review.name);
        assertEquals(description, review.description);
    }

    private static void assertRequesterInfoEquals(PullReview review, String bitbucketName, String displayName) {
        assertEquals(bitbucketName, review.requester.bitbucketUserName);
        assertEquals(displayName, review.requester.name);
    }

}
