package update.pullrequest;

import akka.actor.ActorRef;
import models.PullReviewEvent;
import models.PullReviewEventType;
import models.State;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;

/**
 * PullRequestImporterEventTest
 */
public class PullRequestImporterEventTest extends AbstractPullRequestImporterTest {

    @Test
    public void importAllReviews_publishesPullReviewGeneratedEvents() throws Exception {
        // setup PullRequestImporter actor
        ActorRef pullRequestImporterActor = actorRule.actorOf(PullRequestImporter.class);
        eventStream.subscribe(pullRequestImporterActor, ImportPullRequestsEvent.class);

        // setup receiver
        Future<PullReviewEvent> response = actorRule.expectMsg(PullReviewEvent.class, SECONDS.toMillis(10));

        // trigger pull request import from json
        eventStream.publish(new ImportPullRequestsEvent(firstResponse, repositoryId));

        // verify events sent
        PullReviewEvent reply = Await.result(response, Duration.Inf());
        assertEquals(PullReviewEventType.PULL_REVIEW_GENERATED, reply.pullReviewEventType);
        assertEquals(State.OPEN, reply.pullReview.state);
    }

}
